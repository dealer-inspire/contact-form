<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210723020310 extends AbstractMigration
{
    private const ERROR_MSG = "Migration can only be executed safely on 'mysql'.";

    public function getDescription(): string
    {
        return "Creates the table structure for the contact form.";
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', self::ERROR_MSG);

        $this->addSql(<<<SQL
            CREATE TABLE `contact` (
                `contact_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
                `email` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',
                PRIMARY KEY (`contact_id`),
                UNIQUE INDEX `uq_name_email` (`name`, `email`),
                INDEX `idx_name` (`name`),
                INDEX `idx_email` (`email`)
            )
            COLLATE='utf8_unicode_ci'
            ENGINE=InnoDB
        SQL);

        $this->addSql(<<<SQL
            CREATE TABLE `telephone` (
                `telephone_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `line_number` BIGINT(20) UNSIGNED NOT NULL,
                PRIMARY KEY (`telephone_id`),
                UNIQUE INDEX `uq_line_number` (`line_number`),
                INDEX `idx_line_number` (`line_number`)
            )
            COLLATE='utf8_unicode_ci'
            ENGINE=InnoDB
        SQL);

        $this->addSql(<<<SQL
            CREATE TABLE `message` (
                `message_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `contact_id` INT(10) UNSIGNED NOT NULL,
                `telephone_id` INT(10) UNSIGNED NULL DEFAULT NULL,
                `text` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                PRIMARY KEY (`message_id`),
                INDEX `idx_telephone_id` (`telephone_id`),
                INDEX `idx_message_contact` (`contact_id`),
                CONSTRAINT `fk_message_contact`
                    FOREIGN KEY (`contact_id`)
                    REFERENCES `contact` (`contact_id`)
                    ON UPDATE NO ACTION
                    ON DELETE CASCADE,
                CONSTRAINT `fk_telephone_id`
                    FOREIGN KEY (`telephone_id`)
                    REFERENCES `telephone` (`telephone_id`)
                    ON UPDATE NO ACTION
                    ON DELETE SET NULL
            )
            COLLATE='utf8_unicode_ci'
            ENGINE=InnoDB
        SQL);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DROP TABLE message");
        $this->addSql("DROP TABLE telephone");
        $this->addSql("DROP TABLE contact");
    }
}
