# Dealer Inspire PHP Code Challenge

Here is my code challenge project.

## Getting Started

This repository is stored at `https://gitlab.com/dealer-inspire/contact-form/` and is publicly available.
The command to clone is `https://gitlab.com/dealer-inspire/contact-form.git` via HTTP 
or `git@gitlab.com:dealer-inspire/contact-form.git`.

This challenge was done using the Symfony 5.3 framework, PHP 7.4, and MySQL 8.0. The database access is set up with `root` as the
user name and `password` as the password. Symfony requires the XSL extension to be enabled to send emails 
directly from notifications so please make sure that is enabled.

Please run `composer install` after cloning this project to install all the relevant files and libraries.

After running the command, composer will attempt to run the Doctrine command to install the database and also the 
migration which contains the database architecture. You can also manually run this step later (if you need to delete
the database for any reason) by running `composer install-doctrine`.

PHP Unit is run automatically after running `composer install` but if you need to run it manually for any reason, 
you can use the `composer test` function to run all tests within the application.

You can also run the web server for PHP by running `composer server`.

## Further Reading

If you need to change any of the code (including environmental variables) for any reason you will need to the cache
clear (`composer cache-clear`) command to make sure that the cache is cleared and the application updates accordingly.

The are a few environmental variables that are defined in the `.env` file that can be overwritten if required by
creating an `.env.local` file in the same directory with the names of the parameters you want to replace.

This is currently what it looks it:

```
###> symfony/framework-bundle ###
APP_ENV=prod
APP_SECRET=adba2d6194ab38ed037459d528db6678
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
#
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
DATABASE_URL="mysql://root:password@127.0.0.1:3306/dealer_inspire?serverVersion=8.0"
###< doctrine/doctrine-bundle ###

###> symfony/google-mailer ###
# Gmail SHOULD NOT be used on production, use it in development only.
MAILER_DSN=gmail://gp.bryce.major:xgtqzvyqkpysxzdk@default
###< symfony/google-mailer ###

###> Contact Email ###
CONTACT_EMAIL=guy-smiley@example.com
###< Contact Email ###
```
