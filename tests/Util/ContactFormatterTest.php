<?php

namespace App\Tests\Util;

use App\Components\ContactForm;
use App\Util\ContactFormatter;
use PHPUnit\Framework\TestCase;

class ContactFormatterTest extends TestCase
{
    private const EXPECTED_TEXT_WITH_PHONE = <<<TEXT
            New message for you, Guy!
            
            From: Bryce Major
            Email: bryce.a.major@gmail.com
            Phone: (352) 234-0977
            Message:
                This is a message for Guy Smiley.
        TEXT;

    private const EXPECTED_TEXT_WITHOUT_PHONE = <<<TEXT
            New message for you, Guy!
            
            From: Bryce Major
            Email: bryce.a.major@gmail.com
            Phone: Not provided
            Message:
                This is a message for Guy Smiley.
        TEXT;

    private ContactForm $contactForm;

    public function setUp(): void
    {
        $contactForm = new ContactForm();
        $contactForm->setName("Bryce Major");
        $contactForm->setEmail("bryce.a.major@gmail.com");
        $contactForm->setTelephone("(352) 234-0977");
        $contactForm->setMessage("This is a message for Guy Smiley.");
        $this->contactForm = $contactForm;
    }

    public function testFormat()
    {
        $formatter = new ContactFormatter();
        $result = $formatter->format($this->contactForm);

        $this->assertEquals(self::EXPECTED_TEXT_WITH_PHONE, $result);
    }

    public function testFormatWithoutTelephone()
    {
        $formatter = new ContactFormatter();
        $form = $this->contactForm;
        $form->setTelephone(null);
        $result = $formatter->format($form);

        $this->assertEquals(self::EXPECTED_TEXT_WITHOUT_PHONE, $result);
    }
}
