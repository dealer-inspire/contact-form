<?php

namespace App\Tests\Util;

use App\Components\ContactForm;
use App\Entity\Contact;
use App\Entity\DataObject;
use App\Entity\Message;
use App\Entity\Telephone;
use App\Repository\ContactRepository;
use App\Repository\MessageRepository;
use App\Repository\TelephoneRepository;
use App\Util\ContactFormLoader;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\MethodProphecy;

class ContactFormLoaderTest extends TestCase
{
    public const TEST_DATA = [
        "id" => 1,
        "name" => "Bryce Major",
        "email" => "bryce.a.major@gmail.com",
        "phone" => "(352) 234-0977",
        "strippedPhone" => 3522340977,
        "message" => "This is a message for Guy Smiley."
    ];

    private ContactForm $contactForm;

    public function setUp(): void
    {
        $contactForm = new ContactForm();
        $contactForm->setName("Bryce Major");
        $contactForm->setEmail("bryce.a.major@gmail.com");
        $contactForm->setTelephone("(352) 234-0977");
        $contactForm->setMessage("This is a message for Guy Smiley.");
        $this->contactForm = $contactForm;
    }

    public function testLoadSuccess()
    {
        $contact = new Contact();
        $contact->id = self::TEST_DATA["id"];
        $contact->name = self::TEST_DATA["name"];
        $contact->email = self::TEST_DATA["email"];

        $telephone = new Telephone();
        $telephone->id = self::TEST_DATA["id"];
        $telephone->lineNumber = self::TEST_DATA["strippedPhone"];

        $contactRepo = $this->createMock(ContactRepository::class);
        $contactRepo->expects($this->any())
            ->method("getByName")
            ->willReturn($contact);

        $phoneRepo = $this->createMock(TelephoneRepository::class);
        $phoneRepo->expects($this->any())
            ->method("getByNumber")
            ->willReturn($telephone);

        $messageRepo = $this->createMock(MessageRepository::class);
        $messageRepo->expects($this->any())
            ->method("add")
            ->willReturn(new Message());

        $loader = new ContactFormLoader($contactRepo, $messageRepo, $phoneRepo);

        $this->assertIsBool($loader->load($this->contactForm));
    }

    public function testLoadFailure()
    {
        $contact = new Contact();
        $contact->id = self::TEST_DATA["id"];
        $contact->name = self::TEST_DATA["name"];
        $contact->email = self::TEST_DATA["email"];

        $telephone = new Telephone();
        $telephone->id = self::TEST_DATA["id"];
        $telephone->lineNumber = self::TEST_DATA["strippedPhone"];

        $contactRepo = $this->createMock(ContactRepository::class);
        $contactRepo->expects($this->any())
            ->method("getByName")
            ->willReturn($contact);

        $phoneRepo = $this->createMock(TelephoneRepository::class);
        $phoneRepo->expects($this->any())
            ->method("getByNumber")
            ->willReturn($telephone);

        $messageRepo = $this->createMock(MessageRepository::class);
        $messageRepo->expects($this->any())
            ->method("add")
            ->willReturn(null);

        $loader = new ContactFormLoader($contactRepo, $messageRepo, $phoneRepo);

        $this->assertIsBool($loader->load($this->contactForm));
    }

    public function testLoadWithoutPhoneNumber()
    {
        $contact = new Contact();
        $contact->id = self::TEST_DATA["id"];
        $contact->name = self::TEST_DATA["name"];
        $contact->email = self::TEST_DATA["email"];

        $telephone = new Telephone();
        $telephone->id = self::TEST_DATA["id"];
        $telephone->lineNumber = self::TEST_DATA["strippedPhone"];

        $contactRepo = $this->createMock(ContactRepository::class);
        $contactRepo->expects($this->any())
            ->method("getByName")
            ->willReturn($contact);

        $phoneRepo = $this->createMock(TelephoneRepository::class);
        $phoneRepo->expects($this->any())
            ->method("getByNumber")
            ->willReturn(new Telephone());
        $phoneRepo->expects($this->any())
            ->method("add")
            ->willReturn($telephone);

        $messageRepo = $this->createMock(MessageRepository::class);
        $messageRepo->expects($this->any())
            ->method("add")
            ->willReturn(new Message());

        $loader = new ContactFormLoader($contactRepo, $messageRepo, $phoneRepo);

        $this->assertIsBool($loader->load($this->contactForm));
    }

    public function testLoadNewContact()
    {
        $contact = new Contact();
        $contact->id = self::TEST_DATA["id"];
        $contact->name = self::TEST_DATA["name"];
        $contact->email = self::TEST_DATA["email"];

        $telephone = new Telephone();
        $telephone->id = self::TEST_DATA["id"];
        $telephone->lineNumber = self::TEST_DATA["strippedPhone"];

        $contactRepo = $this->createMock(ContactRepository::class);
        $contactRepo->expects($this->any())
            ->method("getByName")
            ->willReturn(new Contact());
        $contactRepo->expects($this->any())
            ->method("add")
            ->willReturn($contact);

        $phoneRepo = $this->createMock(TelephoneRepository::class);
        $phoneRepo->expects($this->any())
            ->method("getByNumber")
            ->willReturn($telephone);

        $messageRepo = $this->createMock(MessageRepository::class);
        $messageRepo->expects($this->any())
            ->method("add")
            ->willReturn(new Message());

        $loader = new ContactFormLoader($contactRepo, $messageRepo, $phoneRepo);

        $this->assertIsBool($loader->load($this->contactForm));
    }
}
