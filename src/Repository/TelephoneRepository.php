<?php

namespace App\Repository;

use App\Entity\Telephone;
use Doctrine\Persistence\ManagerRegistry;

class TelephoneRepository extends AbstractBaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Telephone::class);
    }

    public function getByNumber(int $phoneNumber): ?Telephone
    {
        return $this->findOneBy(["lineNumber" => $phoneNumber]);
    }
}
