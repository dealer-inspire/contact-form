<?php

namespace App\Repository;

use App\Entity\DataObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Exception;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

abstract class AbstractBaseRepository extends ServiceEntityRepository
{
    /**
     * @throws UnprocessableEntityHttpException
     */
    public function add(DataObject $object): ?DataObject
    {
        $this->getEntityManager()->persist($object);

        try {
            $this->getEntityManager()->flush();
        } catch (Exception $ex) {
            throw new UnprocessableEntityHttpException($ex->getMessage());
        }

        return $object;
    }
}
