<?php

namespace App\Entity;

class Contact implements DataObject
{
    public ?int $id;
    public ?string $name;
    public ?string $email;
}
