<?php

namespace App\Entity;

class Message implements DataObject
{
    public ?int $id;
    public ?string $text;
    public ?Contact $contact;
    public ?Telephone $telephone;
}
