<?php

namespace App\Entity;

class Telephone implements DataObject
{
    public ?int $id;
    public ?int $lineNumber;
}
