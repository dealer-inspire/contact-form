<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Util\ContactFormatter;
use App\Util\ContactFormLoader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;

class IndexController extends AbstractController
{
    public function __invoke(
        Request $request,
        NotifierInterface $notifier,
        ContactFormatter $formatter,
        ContactFormLoader $formLoader
    ): Response {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $recipientEmail = $this->getParameter("app.contact.email");
            $contactDetails = $form->getData();

            // Notify the customer that their message has been sent.
            $this->addFlash(
                "notice",
                "Thank you! Your message was sent to $recipientEmail"
            );

            // Add the data to the database
            if ($formLoader->load($contactDetails) === true) {
                // Create/Send an email using the built in Notification functionality.
                $notification = (new Notification("New Message", ["email"]))->content(
                    $formatter->format($contactDetails)
                );

                // The receiver of the Notification
                $recipient = new Recipient($recipientEmail);

                // Send the notification to the recipient
                $notifier->send($notification, $recipient);
            }
        }

        return $this->render(
            "index.html.twig",
            [
                "contactForm" => $form->createView()
            ]
        );
    }
}
