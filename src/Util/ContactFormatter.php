<?php

namespace App\Util;

use App\Components\ContactForm;

class ContactFormatter
{
    public function format(ContactForm $contactForm): string
    {
        $phone = $contactForm->getTelephone() ?? "Not provided";

        return <<<TEXT
            New message for you, Guy!
            
            From: {$contactForm->getName()}
            Email: {$contactForm->getEmail()}
            Phone: $phone
            Message:
                {$contactForm->getMessage()}
        TEXT;
    }
}
