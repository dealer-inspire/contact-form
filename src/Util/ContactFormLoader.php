<?php

namespace App\Util;

use App\Components\ContactForm;
use App\Entity\Contact;
use App\Entity\Message;
use App\Entity\Telephone;
use App\Repository\ContactRepository;
use App\Repository\MessageRepository;
use App\Repository\TelephoneRepository;

class ContactFormLoader
{
    private ContactRepository $contactRepository;
    private MessageRepository $messageRepository;
    private TelephoneRepository $telephoneRepository;

    public function __construct(
        ContactRepository $contactRepository,
        MessageRepository $messageRepository,
        TelephoneRepository $telephoneRepository
    ) {
        $this->contactRepository = $contactRepository;
        $this->messageRepository = $messageRepository;
        $this->telephoneRepository = $telephoneRepository;
    }

    public function load(ContactForm $contactForm): bool
    {
        $contact = $this->getContact($contactForm->getName(), $contactForm->getEmail());
        $telephone = null;

        if (!empty($contactForm->getTelephone())) {
            $telephone = $this->getTelephone($contactForm->getTelephone());
        }

        $message = new Message();
        $message->contact = $contact;
        $message->telephone = $telephone;
        $message->text = $contactForm->getMessage();

        if ($this->messageRepository->add($message) !== null) {
            return true;
        }

        return false;
    }

    private function getContact(string $name, string $email): Contact
    {
        $contact = $this->contactRepository->getByName($name) ?? new Contact();

        if (!isset($contact->id)) {
            $contact->name = $name;
            $contact->email = $email;
            $contact = $this->contactRepository->add($contact);
        }

        return $contact;
    }

    private function getTelephone(string $telephone): Telephone
    {
        $telephone = preg_replace("/[^0-9]/", "", $telephone);
        $phone = $this->telephoneRepository->getByNumber($telephone) ?? new Telephone();

        if (!isset($phone->id)) {
            $phone->lineNumber = $telephone;
            $phone = $this->telephoneRepository->add($phone);
        }

        return $phone;
    }
}
